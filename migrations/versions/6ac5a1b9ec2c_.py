"""empty message

Revision ID: 6ac5a1b9ec2c
Revises: 6f494f9a37b6
Create Date: 2023-06-22 07:06:05.739124

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6ac5a1b9ec2c'
down_revision = '6f494f9a37b6'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('apikey_permissions', 'product', type_=sa.Enum('BEER', 'COFFEE', 'MATE', name='productenum'), existing_type=sa.Enum('BEER', 'COFFEE', name='productenum'))
    op.alter_column('product_reports', 'product', type_=sa.Enum('BEER', 'COFFEE', 'MATE', name='productenum'), existing_type=sa.Enum('BEER', 'COFFEE', name='productenum'))


def downgrade():
    op.execute("DELETE FROM `apikey_permissions` where product = 'MATE';")
    op.execute("DELETE FROM `product_reports` where product = 'MATE';")
    op.alter_column('apikey_permissions', 'product', type_=sa.Enum('BEER', 'COFFEE', name='productenum'), existing_type=sa.Enum('BEER', 'COFFEE', 'MATE', name='productenum'))
    op.alter_column('product_reports', 'product', type_=sa.Enum('BEER', 'COFFEE', name='productenum'), existing_type=sa.Enum('BEER', 'COFFEE', 'MATE', name='productenum'))
